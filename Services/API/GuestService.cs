﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task02onCore3.Data;
using Task02onCore3.Models.Task02;
using Task02onCore3.Services.Base.Security;

namespace Task02onCore3.Services.API
{
    public class GuestService : AuthSecurity
    {
        public GuestService(Context db) : base(db) { }

        public async Task<LinkModel> CreateGuestLink(string longLink, string serverURL)
        {
            //CreateGuestGroupIfDoesNotExist();
            LinkModel link = new LinkModel
            {
                GroupId = null,
                Name = null,
                RedirectedCount = default,
                Status = true,
                UserID = Guid.Empty,
                Group = null,
                LongLink = longLink,
                ShortLink = await new LinkService(DB).ShortLinkGenerator(4, serverURL)
            };
            DB.Links.Add(link);
            await DB.SaveChangesAsync();
            return link;
        }

        private void CreateGuestGroupIfDoesNotExist()
        {
            if (DB.Groups.Find(Guid.Empty) == null)
            {
                GroupModel group = new GroupModel
                {
                    Name = "Guest",
                    Status = true,
                    UserID = Guid.Empty,
                    Links = null
                };
                DB.Groups.Add(group);
                DB.SaveChanges();
            }
        }
    }
}
