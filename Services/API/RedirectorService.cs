﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Task02onCore3.Data;
using Task02onCore3.Models.Task02;
using Task02onCore3.Services.Base.Security;

namespace Task02onCore3.Services.API
{
    public class RedirectorService : AuthSecurity
    {
        public RedirectorService(Context db) : base(db) { }

        public async Task<string> Redirect(string urlCode)
        {
            LinkModel link = await DB.Links.FirstAsync(p => EF.Functions.Like(p.ShortLink, $"%//%/%/{urlCode}"));

            if (string.IsNullOrEmpty(link.LongLink))
                throw new Exception("LinkNotFound");

            if (link.Status)
            {
                DB.Links.Find(link.Id).RedirectedCount++;
                await DB.SaveChangesAsync();
                return link.LongLink;      
            }
            else
            {
                throw new Exception("LinkIsDisabled");
            }
        }
    }
}
