﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task02onCore3.Data;
using Task02onCore3.Models.Task02;
using Task02onCore3.Models.Task02.Group;
using Task02onCore3.Services.Base;
using Task02onCore3.Services.Base.Security;

namespace Task02onCore3.Services
{
    public class GroupService : AuthSecurity, AppIRepository<GroupModel>
    {
        public GroupService(Context db) : base(db) { }

        public async Task Create(Guid userID, GroupModel item, string t = null)
        {
            if (IsAvailable(userID, item))
            {
                await DB.Groups.AddAsync(item);
                await Save();
            }
            else
                throw new MethodAccessException();
        }

        public async Task Delete(Guid userID, Guid? id)
        {
            GroupModel item = await GetById(userID, id);
            List<LinkModel> links = await DB.Links.Where(p => p.GroupId == id).ToListAsync();
            if (IsAvailable(userID, item))
            {
                //delete all links in this group
                DB.Links.RemoveRange(links);
                //delete group
                DB.Groups.Remove(await GetById(userID, id));
                await Save();
            }
            else
                throw new MethodAccessException();
        }

        public async Task<IEnumerable<GroupModel>> GetAll(Guid userID, Guid? t)
        {
            List<GroupModel> groups = await DB.Groups.Where(p => p.UserID == userID).ToListAsync();
            return IsAvailable(userID, groups) ? groups : throw new MethodAccessException();
        }

        public async Task<GroupModel> GetById(Guid userID, Guid? id)
        {
            GroupModel group = await DB.Groups.FirstOrDefaultAsync(x => x.Id == id);
            return IsAvailable(userID, group) ? group : throw new MethodAccessException();
        }

        public async Task Save()
        {
            await DB.SaveChangesAsync();
        }

        public async Task Update(Guid userID, GroupModel item)
        {
            if (!IsAvailable(userID, item))
                throw new MethodAccessException();

            DB.Entry(item).State = EntityState.Modified;
            await Save();   
        }

        public async Task ChangeStatus(Guid userID, Guid id)
        {
            GroupModel group = await DB.Groups.FirstOrDefaultAsync(x => x.Id == id);
            if (!IsAvailable(userID, group))
                throw new MethodAccessException();

            DB.Groups.FirstOrDefault(p => p.Id == id).Status = !group.Status;
            await Save();

            List<LinkModel> linksInGroup = await DB.Links
                .Where(p => p.GroupId == id)
                .ToListAsync();

            LinkService linkService = new LinkService(DB);

            foreach (var link in linksInGroup)
            {
                await linkService.ChangeStatus(userID, link.Id);
            }
        }

        public async Task<List<GroupIndexDTOModel>> LinksCountInGroups(Guid userID)
        {
            List<GroupIndexDTOModel> groupsDTO = new List<GroupIndexDTOModel>();

            //var groups = await GetAll(userID);
            var groups = DB.Groups.Where(p => p.UserID == userID).ToList<GroupModel>();

            foreach (var group in groups)
            {
                groupsDTO.Add(new GroupIndexDTOModel 
                { 
                    Id = group.Id,
                    LinkCount = LinksCountInGroup(userID, group.Id),
                    LinksRedirected = await LinksRedirectedInGroup(userID, group.Id),
                    Name = group.Name,
                    Status = group.Status,
                    UserID = group.UserID
                });
            }
            return groupsDTO;
        }

        private async Task<int> LinksRedirectedInGroup(Guid userID, Guid groupId)
        {
            return await DB.Links.Where(p => p.UserID == userID)
                .Where(p => p.GroupId == groupId).SumAsync(p => p.RedirectedCount);
        }

        private int LinksCountInGroup(Guid userID, Guid groupId)
        {
            return DB.Links.Where(p => p.UserID == userID).Where(p => p.GroupId == groupId).Count();
        }

        //public Task<IEnumerable<GroupModel>> GetAll(Guid userID, Guid? groupId)
        //{
        //    throw new NotImplementedException();
        //}

        //public Task Create(Guid userID, GroupModel item, Guid groupId, string absoluteServerUrl)
        //{
        //    throw new NotImplementedException();
        //}

        //public Task Create(Guid userID, GroupModel item, string absoluteServerUrl)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
