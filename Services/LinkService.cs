﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task02onCore3.Data;
using Task02onCore3.Models.Task02;
using Task02onCore3.Services.Base;
using Task02onCore3.Services.Base.Security;

namespace Task02onCore3.Services
{
    public class LinkService : AuthSecurity, AppIRepository<LinkModel>
    {
        public LinkService(Context db) : base(db) { }

        public async Task Create(Guid userID, LinkModel item, string absoluteServerUrl)
        {
            item.UserID = userID;
            //item.GroupId = groupId;
            item.ShortLink = await ShortLinkGenerator(4, absoluteServerUrl);

            if (IsAvailable(userID, item))
            {
                await DB.Links.AddAsync(item);
                await Save();
            }
            else
                throw new MethodAccessException();
        }

        public async Task Delete(Guid userID, Guid? id)
        {
            LinkModel item = await GetById(userID, id);
            if (IsAvailable(userID, item))
            {
                DB.Links.Remove(item);
                await Save();
            }
            else
                throw new MethodAccessException();
        }

        public async Task<IEnumerable<LinkModel>> GetAll(Guid userID, Guid? groupId)
        {
            
            List<LinkModel> links = null;
            if (groupId == null)
                links = await DB.Links.Where(p => p.UserID == userID).ToListAsync();
            else
            {
                if (!DB.Groups.Any(p => p.Id == groupId))
                    throw new MethodAccessException();

                links = await DB.Links.Where(p => p.GroupId == groupId).Where(p => p.UserID == userID).ToListAsync();
            }

            return IsAvailable(userID, links) ? links : throw new MethodAccessException();
        }

        public async Task<LinkModel> GetById(Guid userID, Guid? id)
        {
            LinkModel link = await DB.Links.FirstOrDefaultAsync(x => x.Id == id);
            return IsAvailable(userID, link) ? link : throw new MethodAccessException();
        }

        public async Task Save()
        {
            await DB.SaveChangesAsync();
        }

        public async Task Update(Guid userID, LinkModel item)
        {
            if (IsAvailable(userID, item))
            {
                DB.Entry(item).State = EntityState.Modified;
                await Save();
            }
            else throw new MethodAccessException();
        }

        public async Task ChangeStatus(Guid userID, Guid? linkId)
        {
            LinkModel link = DB.Links.Find(linkId);
            GroupModel group = DB.Groups.Find(link.GroupId);
            if ((!group.Status) && (IsAvailable(userID, group)))
            {
                DB.Groups.Find(group.Id).Status = true;
            }
            if (IsAvailable(userID, link))
            {
                DB.Links.Find(link.Id).Status = !link.Status;
                await Save();
            }
            else throw new MethodAccessException();
        }

        public bool IsGroupExistence(Guid userID, Guid? groupID)
        {
            return (DB.Groups.Where(p => p.UserID == userID).Any(p => p.Id == groupID));
        }

        //Рекурсивный метод
        public async Task<string> ShortLinkGenerator(int length, string absoluteServerUrl)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            string linkId = new string(
                Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)])
              .ToArray());

            string shortLink = (absoluteServerUrl + "/r/" + linkId);

            //var tmp = DB.Links.FirstAsync(x => x.ShortLink == shortLink);

            return !(await DB.Links.AnyAsync(x => x.ShortLink == shortLink))
                ? shortLink 
                : await ShortLinkGenerator(++length, absoluteServerUrl);
        }
    }
}
