﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task02onCore3.Data;
using Task02onCore3.Models.Task02;

namespace Task02onCore3.Services.Base.Security
{
    public class AuthSecurity : AppBaseService
    {
        public AuthSecurity(Context db) : base(db) { }

        public bool IsAvailable(Guid userID, List<LinkModel> links)
        {
            foreach (var link in links)
                if (link.UserID != userID)
                    return false;
            return true;
        }

        public bool IsAvailable(Guid userID, List<GroupModel> groups)
        {
            foreach (var group in groups)
                if (group.UserID != userID)
                    return false;
            return true;
        }

        public bool IsAvailable(Guid userID, LinkModel link)
        {
            if (link.UserID != userID)
                return false;
            return true;
        }

        public bool IsAvailable(Guid userID, GroupModel group)
        {
            if (group.UserID != userID)
                return false;
            return true;
        }
    }
}
