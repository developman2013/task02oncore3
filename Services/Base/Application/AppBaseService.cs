﻿using Task02onCore3.Data;

namespace Task02onCore3.Services.Base
{
    public abstract class AppBaseService
    {
        public AppBaseService(Context db) { DB = db; }
        protected Context DB { get; }
    }
}
