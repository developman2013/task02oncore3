﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Task02onCore3.Services.Base
{
    interface AppIRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAll(Guid userID, Guid? groupId);
        Task<T> GetById(Guid userID, Guid? id);
        Task Create(Guid userID, T item, string absoluteServerUrl);
        Task Update(Guid userID, T item);
        Task Delete(Guid userID, Guid? id);
        Task Save();
    }
}
