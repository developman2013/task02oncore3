﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task02onCore3.Data;

namespace Task02onCore3.Services.Base.API
{
    public abstract class ApiBaseService 
    {
        public ApiBaseService(Context db) { Db = db; }
        protected Context Db { get; }
    }
}
