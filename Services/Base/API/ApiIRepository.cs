﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task02onCore3.Services.Base.API
{
    interface ApiIRepository
    {
        Task Redirect(Uri uri);
    }
}
