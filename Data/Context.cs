﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task02onCore3.Models.Task02;

namespace Task02onCore3.Data
{
    public class Context : IdentityDbContext<IdentityUser>
    {
        public Context(DbContextOptions options) : base(options)
        {
            //Deteled database
            //Database.EnsureDeleted();

            //Create database
            Database.EnsureCreated();
        }

        public DbSet<LinkModel> Links { get; set; }
        public DbSet<GroupModel> Groups { get; set; }
    }
}
