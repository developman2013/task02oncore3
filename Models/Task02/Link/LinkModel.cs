﻿using System;

namespace Task02onCore3.Models.Task02
{
    public class LinkModel
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public Guid UserID { get; set; }
        public string LongLink { get; set; }
        public string ShortLink { get; set; }
        public int RedirectedCount { get; set; }
        public bool Status { get; set; }

        public Guid? GroupId { get; set; }
        public GroupModel Group { get; set; }
    }
}
