﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task02onCore3.Models.Task02.Group
{
    public class GroupIndexDTOModel : GroupModel
    {
        public int LinkCount { get; set; }
        public int LinksRedirected { get; set; }
    }
}
