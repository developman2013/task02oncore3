﻿using System;
using System.Collections.Generic;

namespace Task02onCore3.Models.Task02
{
    public class GroupModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid UserID { get; set; }
        public bool Status { get; set; }
        public List<LinkModel> Links { get; set; }
    }
}
