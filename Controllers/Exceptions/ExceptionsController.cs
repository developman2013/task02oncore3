﻿using Microsoft.AspNetCore.Mvc;

namespace Task02onCore3.Controllers.Exceptions
{
    public class ExceptionsController : Controller
    {
        public IActionResult Index(int code)
        {
            switch (code)
            {
                case 403:
                    return Forbid(code);
                case 404:
                    return PageNotFound(code);
                case 500:
                    return SomethingWrong(code);
                default:
                    return SomethingWrong(code);
            }
        }

        public IActionResult Forbid(int code)
        {
            Response.StatusCode = code;
            return View("Forbid");
        }

        public IActionResult PageNotFound(int code)
        {
            Response.StatusCode = code;
            return View("PageNotFound");
        }

        public IActionResult LinkNotFound(int code)
        {
            Response.StatusCode = code;
            return View("LinkNotFound");
        }

        public IActionResult LinkIsDisabled(int code)
        {
            Response.StatusCode = code;
            return View("LinkIsDisabled");
        }

        public IActionResult BadURL(int code)
        {
            Response.StatusCode = code;
            return View("BadRequest");
        }

        public IActionResult SomethingWrong(int code)
        {
            Response.StatusCode = code;
            return View("SomethingWrong");
        }
    }
}