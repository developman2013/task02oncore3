﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task02onCore3.Data;
using Task02onCore3.Models.Task02;
using Task02onCore3.Services;

namespace Task02onCore3.Controllers
{
    [Authorize]
    public class LinkController : Controller
    {
        private readonly LinkService linkService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly Guid UserID;

        public LinkController(Context db, DbContextOptions<Context> options, IHttpContextAccessor httpContextAccessor)
        {
            linkService = new LinkService(db);
            _httpContextAccessor = httpContextAccessor;
            UserID = Guid.Parse(_httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
        }

        // GET: Link
        public async Task<IActionResult> Index(Guid? groupID)
        {
            try
            {
                ViewBag.groupID = groupID;
                return View(await linkService.GetAll(UserID, groupID));
            }
            catch (MethodAccessException)
            {
                return Forbid();
            }
        }

        // GET: Link/Details/5
        //public async Task<IActionResult> Details(Guid id)
        //{
        //    LinkModel linkModel;
        //    try
        //    {
        //        linkModel = await linkService.GetById(UserID, id);
        //        if (linkModel == null)
        //        {
        //            return NotFound();
        //        }

        //        return View(linkModel);
        //    }
        //    catch(MethodAccessException)
        //    {
        //        return Forbid();
        //    }
        //}

        // GET: Link/Create
        public IActionResult Create(Guid groupId)
        {
            try
            {
                if (!linkService.IsGroupExistence(UserID,groupId))
                    throw new MethodAccessException();
                ViewBag.GroupId = groupId;
                return View();
            }
            catch (MethodAccessException)
            {
                return Forbid();
            }
        }

        // POST: Link/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(LinkModel item)
        {
            try
            {
                if (!linkService.IsGroupExistence(UserID, item.GroupId))
                    throw new MethodAccessException();
                item.UserID = UserID;
                item.Id = Guid.NewGuid();
                item.Status = true;
                if (ModelState.IsValid)
                {
                    string absoluteServerUrl = string.Format("{0}://{1}", Request.Scheme, Request.Host);
                    await linkService.Create(UserID, item, absoluteServerUrl);
                    return RedirectToAction("Index", "Link", new { item.GroupId});
                }
                return View(item);
            }
            catch (MethodAccessException)            
            {
                return Forbid();
            }
        }

        // GET: Link/Delete/5
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                await linkService.Delete(UserID, id);
                return Redirect(Request.Headers["Referer"].ToString());
            }
            catch (MethodAccessException)
            {
                return Forbid();
            }
        }

        public async Task<ActionResult> ChangeStatus(Guid linkId)
        {
            try
            {
                await linkService.ChangeStatus(UserID, linkId);
                //return RedirectToAction("Index", "Group");
                //return RedirectToAction(nameof(Index));

                return Redirect(Request.Headers["Referer"].ToString());
            }
            catch(MethodAccessException)
            {
                return Forbid();   
            }

        }
    }
}
