﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Task02onCore3.Data;
using Task02onCore3.Services.API;

namespace Task02onCore3.Controllers.Redirector
{
    //[Route("r/")]
    [ApiController]
    public class rController : ControllerBase
    {
        private readonly RedirectorService redirectorService;

        public rController(Context db)
        {
            redirectorService = new RedirectorService(db);
        }

        [Route("r/{urlCode}")]
        public async Task<ActionResult> Redirector(string urlCode)
        {
            try
            {
                return Redirect(await redirectorService.Redirect(urlCode));      
            }
            catch(Exception ex)
            {
                return RedirectToAction(ex.Message, "Exceptions");   
            }


        }

    }
}
