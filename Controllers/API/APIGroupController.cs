﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task02onCore3.Data;
using Task02onCore3.Models.Task02;
using Task02onCore3.Services;

namespace Task02onCore3.Controllers.API
{
    [Route("api/group/")]
    [ApiController]
    public class APIGroupController : ControllerBase
    {
        private readonly GroupService groupService;
        private readonly Guid UserID;

        public APIGroupController(Context db, DbContextOptions<Context> options, IHttpContextAccessor httpContextAccessor)
        {
            groupService = new GroupService(db);
            UserID = Guid.Parse(httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            GroupModel group = await groupService.GetById(UserID, id);
            await groupService.Delete(UserID, group.Id);
            group.Links = null;
            return Ok(group);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> ChangeStatus(Guid id)
        {
            GroupModel group = await groupService.GetById(UserID, id);
            await groupService.ChangeStatus(UserID, group.Id);
            group.Links = null;
            return Ok(group);
        }
    }
}
