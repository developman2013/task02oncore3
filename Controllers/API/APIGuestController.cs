﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task02onCore3.Data;
using Task02onCore3.Models.Task02;
using Task02onCore3.Models.Task02.API;
using Task02onCore3.Services;
using Task02onCore3.Services.API;

namespace Task02onCore3.Controllers.API
{
    [Route("api/guest/")]
    [ApiController]
    public class APIGuestController : ControllerBase
    {
        private readonly GuestService guestSerivice;

        public APIGuestController(Context db, DbContextOptions<Context> options, IHttpContextAccessor httpContextAccessor)
        {
            guestSerivice = new GuestService(db);
        }

        // PUT: api/APIGuest/5
        public async Task<IActionResult> Put([FromBody] LinkInModel longLink)
        {
            string a = longLink.LongLink;

            LinkModel link = await guestSerivice.CreateGuestLink(
                longLink.LongLink,
                string.Format("{0}://{1}", Request.Scheme, Request.Host)
                );
            LinkModel tmp = link;
            return Ok(link);
        }
    }
}
