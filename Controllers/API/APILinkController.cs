﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task02onCore3.Data;
using Task02onCore3.Models.Task02;
using Task02onCore3.Services;

namespace Task02onCore3.Controllers.API
{
    [Route("api/link/")]
    [ApiController]
    public class APILinkController : ControllerBase
    {
        private readonly LinkService linkSerivice;
        private readonly Guid UserID;

        public APILinkController(Context db, DbContextOptions<Context> options, IHttpContextAccessor httpContextAccessor)
        {
            linkSerivice = new LinkService(db);
            UserID = Guid.Parse(httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
        }

        //// GET: api/APILink
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET: api/APILink/5
        //[HttpGet("{id}", Name = "Get")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST: api/APILink
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT: api/APILink/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        [HttpPut("{id}")]
        public async Task<IActionResult> ChangeStatus(Guid id)
        {
            LinkModel link = await linkSerivice.GetById(UserID, id);
            await linkSerivice.ChangeStatus(UserID, link.Id);
            return Ok(link);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            LinkModel link = await linkSerivice.GetById(UserID, id);
            await linkSerivice.Delete(UserID, link.Id);
            return Ok(link);
        }
    }
}
