﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task02onCore3.Data;
using Task02onCore3.Models.Task02;
using Task02onCore3.Services;

namespace Task02onCore3.Controllers
{
    [Authorize]
    public class GroupController : Controller
    {
        private readonly GroupService groupService;
        private readonly Guid UserID;

        public GroupController(Context db, DbContextOptions<Context> options, IHttpContextAccessor httpContextAccessor)
        {
            groupService = new GroupService(db);
            UserID = Guid.Parse(httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
        }

        // GET: Group
        public async Task<IActionResult> Index()
        {
            //ViewBag.Counts = groupService.LinksCountInGroups(UserID);
            return View(await groupService.LinksCountInGroups(UserID));
        }

        // GET: Group/Details/5
        //public async Task<IActionResult> Details(Guid id)
        //{
        //    GroupModel item;
        //    try
        //    {

        //        item = await groupService.GetById(UserID, id);
        //        if (item == null)
        //        {
        //            return NotFound();
        //        }

        //        return View(item);
        //    }
        //    catch (MethodAccessException)
        //    {
        //        return Forbid();
        //    }
        //}

        // GET: Group/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Group/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(string test, GroupModel item)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string tmp = test;

                    item.Id = Guid.NewGuid();
                    item.UserID = UserID;
                    item.Status = true;
                    await groupService.Create(UserID, item);
                    return RedirectToAction(nameof(Index));
                }
                return View(item);
            }
            catch (MethodAccessException)
            {
                return Forbid();
            }
            
        }

        // GET: Group/Edit/5
        //public async Task<IActionResult> Edit(Guid? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var groupModel = await _context.Groups.FindAsync(id);
        //    if (groupModel == null)
        //    {
        //        return NotFound();
        //    }
        //    return View(groupModel);
        //}

        //// POST: Group/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(Guid id, [Bind("Id,Name,UserName,Status")] GroupModel groupModel)
        //{
        //    if (id != groupModel.Id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(groupModel);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!GroupModelExists(groupModel.Id))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View(groupModel);
        //}

        // GET: Group/Delete/5
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                await groupService.Delete(UserID, id);
                return Redirect(Request.Headers["Referer"].ToString());
            }
            catch (MethodAccessException)
            {
                return Forbid();
            }
        }

        // POST: Group/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(Guid id)
        //{
        //    var groupModel = await _context.Groups.FindAsync(id);
        //    _context.Groups.Remove(groupModel);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}

        //private bool GroupModelExists(Guid id)
        //{
        //    return _context.Groups.Any(e => e.Id == id);
        //}

        public async Task<IActionResult> ChangeStatus(Guid id)
        {
            try
            {
                await groupService.ChangeStatus(UserID, id);
                return RedirectToAction(nameof(Index));
            }
            catch (MethodAccessException)
            {
                return Forbid();
            }
        }

    }
}
